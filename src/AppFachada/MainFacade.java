package AppFachada;

import Programa.ConfigCondominio.AppGastoCondominio;
import Programa.ConfigGastos.AppGastos;
import Programa.FormatoVentanas.AppIngreso;
import Programa.ConfigCondominio.AppConfigCondominio;


import java.util.Scanner;

public class MainFacade {

    public static void main(String[] args) {
        // Métodos a utilizar en la Fachada
        AppIngreso ingreso1 = new AppIngreso();
        AppConfigCondominio appConfigCondominio = new AppConfigCondominio();
        AppGastos appGastos = new AppGastos();
        AppGastoCondominio appGastoCondominio=new AppGastoCondominio();


        ingreso1.mostrarMenu0();
        System.out.print("|  >>>>Ingrese el número de la opción elegida:");
        Scanner sc = new Scanner(System.in);
        int moduloElegido = sc.nextInt();

        if(moduloElegido == 1){
            appConfigCondominio.mostrarMenuAppConfigCondominio();
        } if(moduloElegido == 2){
            appGastos.MostrarMenuGastos();
        } if(moduloElegido == 3){
            appGastoCondominio.mostrarMenuGastosCondominio();
        } if(moduloElegido == 4){
           // modulo 4
        }

    }


}

