package Programa.ConfigCondominio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class CargarDpto {
    public static String[][] cargar_array_csv(String archivo) throws IOException {
        Path ruta = Paths.get(archivo);
        List<String> lines = Files.readAllLines(ruta);
        String[] header = lines.get(0).split(",");
        String[][] datos = new String[lines.size()][header.length];
        int i;
        for (i = 0; i < lines.size(); i++) {
            datos[i] = lines.get(i).split(",");
        }
        return datos;
    }
}
