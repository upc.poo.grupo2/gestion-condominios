package Programa.ConfigCondominio;

import java.util.ArrayList;

public class Departamento extends Condominio {
    private String sector; //la torre o el bloque donde esta el departamento
    private String numero_dpto; // numero de departamento
    private double area_dpto;
    private String propietario_dpto;
    private String habitante_dpto;
    private int cant_cocheras;
    private double area_cocheras;
    private String detalle;
    private double area_total;
    private double factor;
    private static ArrayList<Departamento> dpto_sector = new ArrayList<>(); //= new ArrayList<>();

    public Departamento(String codigo_condominio, String sector, String numero_dpto, double area_dpto, String propietario_dpto) {
        super(codigo_condominio);
        this.sector = sector;
        this.numero_dpto = numero_dpto;
        this.area_dpto = area_dpto;
        this.propietario_dpto = propietario_dpto;
        dpto_sector.add(this);
    }

    public static ArrayList<Departamento> getDpto_sector() {
        return dpto_sector;
    }

    public void setHabitante_dpto(String habitante_dpto) {
        this.habitante_dpto = habitante_dpto;
    }

    public String getSector() {
        return sector;
    }

    public String getNumero_dpto() {
        return numero_dpto;
    }

    public String getHabitante_dpto() {
        return habitante_dpto;
    }

    public String getPropietario_dpto() {
        return propietario_dpto;
    }

    public void setPropietario_dpto(String propietario_dpto) { this.propietario_dpto = propietario_dpto; }

    public void setCant_cocheras(int cant_cocheras) {
        this.cant_cocheras = cant_cocheras;
    }

    public void setArea_cocheras(double area_cocheras) {
        this.area_cocheras = area_cocheras;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public void setArea_total(double area_total) {
        this.area_total = area_total;
    }

    public void setFactor(double factor) {
        this.factor = factor;
    }

    public double getArea_dpto() {
        return area_dpto;
    }

    public double getArea_cocheras() {
        return area_cocheras;
    }

    public double getArea_total() {
        return area_total;
    }

    public double getFactor() {
        return factor;
    }

    public void setArea_dpto(double area_dpto) {
        this.area_dpto = area_dpto;
    }

    @Override
    public String toString() {
        if (factor == 0.0) { // si factor no esta calculado
            return "Dpto {" +
                    "sector='" + sector + '\'' +
                    ", numero='" + numero_dpto + '\'' +
                    ", area=" + area_dpto +
                    ", propietario='" + propietario_dpto + '\'' +
                    ", habitante='" + (habitante_dpto == null ? "NA" : habitante_dpto) + '\'' +
                    ", cocheras='" + (detalle == null ? "NA" : detalle) + '\'' +
                    '}' + '\n';
        } else {
            return "Dpto {" +
                    "sector='" + sector + '\'' +
                    ", numero='" + numero_dpto + '\'' +
                    ", area=" + area_dpto +
                    ", propietario='" + propietario_dpto + '\'' +
                    ", habitante='" + (habitante_dpto == null ? "NA" : habitante_dpto) + '\'' +
                    ", num_cocheras='" + cant_cocheras + '\'' +
                    ", area_cochera='" + area_cocheras + '\'' +
                    ", cocheras='" + (detalle == null ? "NA" : detalle) + '\'' +
                    ", area_total='" + area_total + '\'' +
                    ", factor='" + factor + '\'' +
                    '}' + '\n';

        }
    }
}


/*
    @Override
    public String toString() {
        return "Dpto {" +
                "sector='" + sector + '\'' +
                ", numero='" + numero_dpto + '\'' +
                ", area=" + area_dpto +
                ", propietario='" + propietario_dpto + '\'' +
                ", habitante='" + (habitante_dpto==null?"NA":habitante_dpto) + '\'' +
                '}' + '\n';
    }

 */

    /*
    public void verdpto () {
    System.out.println(Departamento.dpto_sector);
    }
    */
