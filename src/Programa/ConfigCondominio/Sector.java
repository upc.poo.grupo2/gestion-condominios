package Programa.ConfigCondominio;

import java.util.ArrayList;

public class Sector  extends Condominio {
    private String cod_sector;
    private String nombre_sector;
    private int cant_dpto;
    private static ArrayList<Sector> lista_sectores= new ArrayList<>();

    public Sector(String codigo_condominio,  String cod_sector, String nombre_sector, int cant_dpto) {
        super(codigo_condominio);
        this.cod_sector = cod_sector;
        this.nombre_sector = nombre_sector;
        this.cant_dpto = cant_dpto;
        lista_sectores.add(this);
    }

    public void setNombre_sector(String nombre_sector) {
        this.nombre_sector = nombre_sector;
    }

    public void setCant_dpto(int cant_dpto) {
        this.cant_dpto = cant_dpto;
    }

    public String getCod_sector() {
        return cod_sector;
    }

    public int getCant_dpto() {
        return cant_dpto;
    }

    public static ArrayList<Sector> getLista_sectores() {
        return lista_sectores;
    }

    // metodo devuelve cantidad de dptos en una torre
    public int num_dptosSector (String cod_sector){
        int num_dp=0;
        ArrayList<Sector> test =Sector.getLista_sectores();
        for (Sector objX : test){
            if(objX.getCod_sector().equals(cod_sector)){
                num_dp = objX.getCant_dpto();
                break;
            }
        }
        return num_dp;
    }

    // metodo devuelve cantidad de dptos en el arreglo "lista_sectores" osea el total del condominio
    public static int num_dptos(){
        int num_dp=0;
        ArrayList<Sector> test =Sector.getLista_sectores();
        for (Sector objX : test){
                num_dp = num_dp + objX.getCant_dpto();
            }
        return num_dp;
    }
/*
    public void setNombre_sector(String nombre_sector) {
        this.nombre_sector = nombre_sector;
    }

    public void setCant_dpto(int cant_dpto) {
        this.cant_dpto = cant_dpto;
    }

 */
}


///  [{c001, a, torra a, 10},{c001,b,torreb ,15}]