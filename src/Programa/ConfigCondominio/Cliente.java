package Programa.ConfigCondominio;
import Programa.ConfigCondominio.Condominio;

import java.util.ArrayList;

public class Cliente {
    private String codigo_cliente;
    private String nombre_cliente;
    private String RUC;
    private ArrayList <Condominio> ListaCondominios;

    public Cliente(String codigo_cliente, String nombre_cliente) {
        this.codigo_cliente = codigo_cliente;
        this.nombre_cliente = nombre_cliente;
        ListaCondominios = new ArrayList<>();
    }

    public Cliente(){}

    public String getRUC() {
        return RUC;
    }

    public String getCodigo_cliente() {
        return codigo_cliente;
    }

    public String getNombre_cliente() {
        return nombre_cliente;
    }

    public ArrayList<Condominio> getListaCondominios() {
        return ListaCondominios;
    }

    public void setRUC(String RUC) {
        this.RUC = RUC;
    }

    public void agregarCondominio (Condominio x) {
        ListaCondominios.add(x);
    }

    public void setListaCondominios(ArrayList<Condominio> listaCondominios) {
        ListaCondominios = listaCondominios;
    }

    @Override
    public String toString() {
        String text;
        if (RUC == null){
            text = "n/a";
        } else text = "" + RUC;
        return "clientes poo_fm {" +
                "codigo=" + codigo_cliente +
                ", Nombre=" + nombre_cliente +
                ", RUC=" + text +
                ", ListaCondominios=" + ListaCondominios +
                '}';
    }

    // metodo para calcular la facturación de la empresa
    // en funcion al numero departamentos que tenga registrado el cliente, si tiene mas de un condominio registrado descuento adicional por volumen

}
