package Programa.ConfigCondominio;

import Programa.Excepciones.ExcepcionNoEncuentraDocumento;
import Programa.Excepciones.ExcepcionRecibosEmitidos;

import java.util.ArrayList;
import static Programa.ConfigGastos.GastoComun.TotalxPeriodoxtorre;

public class GastoDepartamento implements recibosCondominio { // los recibos pendientes no se acumulan en el periodo vigente set "2109" oct "2110" se pagan por separado
    private String numRecibo;
    private String periodo;
    private String dpto;
    private String propietario;
    private double montoDpto;
    private String estado; // pendiente o cancelaoo
    private static ArrayList<GastoDepartamento> lista_recibos; // = new ArrayList<>();
    private static ArrayList <GastoDepartamento> control_recibos = new ArrayList<>();

    public GastoDepartamento () {
        lista_recibos.add(this);
    }

    public String getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(String numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getDpto() {
        return dpto;
    }

    public void setDpto(String dpto) {
        this.dpto = dpto;
    }

    public String getPropietario() {
        return propietario;
    }

    public void setPropietario(String propietario) {
        this.propietario = propietario;
    }

    public double getMontoDpto() {
        return montoDpto;
    }

    public void setMontoDpto(double montoDpto) {
        this.montoDpto = montoDpto;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public static ArrayList<GastoDepartamento> getLista_recibos() {
        return lista_recibos;
    }

    public static ArrayList<GastoDepartamento> getControl_recibos() {
        return control_recibos;
    }

    public void setControl_recibos(ArrayList<GastoDepartamento> control_recibos) {
        this.control_recibos = control_recibos;
    }


    public static void emitirRecibos (String periodo) throws ExcepcionRecibosEmitidos {
        boolean duplicada = false;
        for (GastoDepartamento objGasto : control_recibos){
            if (String.valueOf(objGasto.getPeriodo()).equals(periodo)){
                throw new ExcepcionRecibosEmitidos("OPERACION CANCELADA: Los recibos del periodo "+periodo + " ya existen");
            }
        } if (!duplicada) {
            lista_recibos = new ArrayList<>();
            int cantidad = Sector.num_dptos(); // cantidad de dptos en el condominio
            recibosFactory recibos = new recibosFactory();
            for (int x = 0; x < cantidad; x++) {
                recibosCondominio cantDpto = recibos.getrecibosCondominio();
            }
            int x = 0;
            for (Departamento objDpto : Departamento.getDpto_sector()) {
                String dpto = objDpto.getSector() + objDpto.getNumero_dpto();
                String recibo = periodo + "-" + dpto;
                GastoDepartamento.getLista_recibos().get(x).setNumRecibo(recibo);
                GastoDepartamento.getLista_recibos().get(x).setPeriodo(periodo);
                GastoDepartamento.getLista_recibos().get(x).setDpto(dpto);
                GastoDepartamento.getLista_recibos().get(x).setPropietario(objDpto.getPropietario_dpto());
                GastoDepartamento.getLista_recibos().get(x).setEstado("Pendiente");
                //total comun 2109 : System.out.println(TotalxPeriodoxtorre("2109","comun"));
                double montocomun=objDpto.getFactor()*TotalxPeriodoxtorre(periodo,"comun");
                double montotorre=TotalxPeriodoxtorre(periodo,objDpto.getSector())/Sector.num_dptos();
                GastoDepartamento.getLista_recibos().get(x).setMontoDpto(Math.round((montocomun+montotorre)*100)/100);
                x = x + 1;
            }
            // envia los recibo emitidos del periodo indicado en el parametro control_recibos (historial)
            control_recibos.addAll(lista_recibos);
            // se utiliza lista_recibos para emitir el recibo y control_recibos para guardar el historial
            System.out.println(control_recibos.toString());
        }
    }

    public static void buscarRecibo (String periodo,String sector,String numdpto) throws ExcepcionNoEncuentraDocumento {
        boolean encontrado = false;
        String recibo = periodo + "-" + sector + numdpto;
        for (GastoDepartamento objGasto : control_recibos) {
            //System.out.println(objGasto.getNumRecibo());
            //System.out.println(recibo);
            if (String.valueOf(objGasto.getNumRecibo()).equals(recibo)) {
                System.out.println(objGasto.toString());
                encontrado = true;
                break;
            }
        }
            if (!encontrado){
                throw new ExcepcionNoEncuentraDocumento(
                        "El recibo " + recibo + " no se encuentra registrado."
                );
            }
        }

        public static void cambiarEstadoRecibo (String periodo,String sector,String numdpto, String estado) throws ExcepcionNoEncuentraDocumento{
            boolean encontrado = false;
            String recibo = periodo + "-" + sector + numdpto;
            for (GastoDepartamento objGasto : control_recibos) {
                //System.out.println(objGasto.getNumRecibo());
                //System.out.println(recibo);
                if (String.valueOf(objGasto.getNumRecibo()).equals(recibo)) {
                    objGasto.setEstado(estado);
                    System.out.println(objGasto.toString());
                    encontrado = true;
                    break;
                }
            }
            if (!encontrado){
                throw new ExcepcionNoEncuentraDocumento(
                        "El recibo " + recibo + " no se encuentra registrado."
                );
            }
        }


    @Override
    public void test() {

    }

    @Override
    public String toString() {
        return "CtaDpto{" +
                "numRecibo='" + numRecibo + '\'' +
                ", periodo='" + periodo + '\'' +
                ", dpto='" + dpto + '\'' +
                ", propietario='" + propietario + '\'' +
                ", montoDpto=" + montoDpto +
                ", estado='" + estado + '\'' +
                '}' + '\n';
    }

    // recibo = periodo & "-" & dpto
    //suma gastos comunes x factor dpto = A
    //suma gastos x torre / dptos x torre = B
    //monto del recibo por dpto = A + B
    // public double TotalxPeriodo(int mes,int anio)
    // public double TotalxPeriodoxtorre(int mes,int anio,String torre){


}
/*
    public static void emitirRecibos (String periodo){
        int cantidad = Sector.num_dptos(); // cantidad de dpto en el condominio
        recibosFactory recibos = new recibosFactory();
        for (int x=0; x<cantidad;x++){
        recibosCondominio cantDpto = recibos.getrecibosCondominio();
            for (Departamento objDpto : Departamento.getDpto_sector()){
                String recibo = periodo + "-"+ objDpto.getSector() + objDpto.getNumero_dpto();
                GastoDepartamento.getLista_recibos().get(x).setNumRecibo(recibo);
 */

      /*
       public static void emitirRecibos (String periodo){
        int cantidad = Sector.num_dptos(); // cantidad de dpto en el condominio
        recibosFactory recibos = new recibosFactory();
        for (int x=0; x<cantidad;x++) {
        recibosCondominio cantDpto = recibos.getrecibosCondominio();
        }
        for (int x=0; x<Departamento.getDpto_sector().size();x++) {
        String recibo = periodo + "-"+ Departamento.getDpto_sector().get(x).getSector() + Departamento.getDpto_sector().get(x).getNumero_dpto();
        GastoDepartamento.getLista_recibos().get(x).setNumRecibo(recibo);
        */
