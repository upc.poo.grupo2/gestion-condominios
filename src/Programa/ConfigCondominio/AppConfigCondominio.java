package Programa.ConfigCondominio;

import Programa.Excepciones.ExcepcionNoEncuentraValorBuscado;
import Programa.Excepciones.ExcepcionEsperar;
import Programa.FormatoVentanas.AppIngreso;

import Programa.ConfigPersona.Persona;

import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class AppConfigCondominio {

    public void mostrarMenuAppConfigCondominio() {
        AppIngreso ingreso1 = new AppIngreso();
        ExcepcionEsperar excepcionEsperar = new ExcepcionEsperar();

        System.out.println("_____________________________________________________________________________________________________________________________________________________________________|");
        System.out.println("MÓDULO DE CONDOMINIOS                                                                                                                                                |");
        System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------|");
        System.out.println("Por favor elija la acción a utilizar:");
        System.out.println("0: IR AL MENÚ PRINCIPAL");
        System.out.println("1: CREAR CONDOMINIOS");
        System.out.println("2: CREAR/ACTUALIZAR SECTORES");
        System.out.println("3: CREAR/ACTUALIZAR DEPARTAMENTOS");
        System.out.println("4: CREAR/ACTUALIZAR COCHERAS");
        System.out.println("5: REGISTRO DE ÁREAS COMUNES");
        System.out.println("6: LISTAR DEPARTAMENTOS");
        System.out.println("7: LISTAR COCHERAS");
        System.out.println("8: LISTAR ÁREAS COMUNES");
        System.out.println("9: CARGA MASIVA ");
        //System.out.println("10: ");

        Scanner sc2 = new Scanner(System.in);
        System.out.print(">>>>Ingrese la opción elegida: ");
        int opcionSubModulo  = sc2.nextInt();


        if(opcionSubModulo == 0){
        ingreso1.mostrarMenu2();
        }if(opcionSubModulo == 1){
        registrarCondominio();
        mostrarMenuAppConfigCondominio();
        }if(opcionSubModulo == 2){
        crear_ActualizarSector();
        mostrarMenuAppConfigCondominio();
        } if(opcionSubModulo == 3){
        crear_ActualizarDpto();
        mostrarMenuAppConfigCondominio();
        } if(opcionSubModulo == 4){
        crear_ActualizarCochera();
        mostrarMenuAppConfigCondominio();
        } if(opcionSubModulo == 5){
        registrarAreaComun();
        mostrarMenuAppConfigCondominio();

        } if(opcionSubModulo == 6){
        mostrarDepartamentos();
        excepcionEsperar.ExcepcionEsperarTeclado();
        mostrarMenuAppConfigCondominio();
        } if(opcionSubModulo == 7){
        mostrarCocheras();
        excepcionEsperar.ExcepcionEsperarTeclado();
        mostrarMenuAppConfigCondominio();
        }if(opcionSubModulo == 8){
        mostrarZonasComunes();
        excepcionEsperar.ExcepcionEsperarTeclado();
        mostrarMenuAppConfigCondominio();
        }if(opcionSubModulo == 9){
            reporteGeneral();
            mostrarMenuAppConfigCondominio();
        }if(opcionSubModulo == 10){
            reporteGeneral();
            mostrarMenuAppConfigCondominio();
        }

    }

    public void registrarCondominio() {
        int seguir = 1;

        Scanner sc = new Scanner(System.in);
        System.out.println("========= INGRESAR CONDOMINIO==================================================");
        System.out.print("Codigo de Condominio:");
        String codCondominio = sc.nextLine();
        System.out.print("Nombre de Condominio:");
        String nombCondominio = sc.nextLine();
        Scanner sc2 = new Scanner(System.in);
        System.out.print("Dirección:");
        String direcCondominio = sc2.nextLine();
        Condominio Cx = new Condominio(codCondominio,nombCondominio,direcCondominio);
        System.out.print("¿Desea registrar otro condominio? (Sí:1 || No:0) : ");
        seguir  = sc2.nextInt();

        if(seguir==1){ registrarCondominio();
        }

    }

    public void crear_ActualizarSector(){
        int seguirRegistro = 1;
        Scanner sc = new Scanner(System.in);
        System.out.println("========= CREAR/ACTUALIZAR SECTOR==================================================");
        System.out.print("Tipo de registro (1: Crear | 2:Actualizar ):");
        int tipoRegistro = sc.nextInt();

        Scanner sc10 = new Scanner(System.in);
        System.out.print("Codigo de condominio:");
        String codCondominio = sc10.nextLine();

        Scanner sc11 = new Scanner(System.in);
        System.out.print("Código de Sector:");
        String codSector = sc11.nextLine();

        Scanner sc13 = new Scanner(System.in);
        System.out.print("Nombre del sector:");
        String nombresector = sc13.nextLine();

        Scanner sc2 = new Scanner(System.in);
        System.out.print("Cantidad de departamentos:");
        int cantDepartamentos = sc2.nextInt();

        if(tipoRegistro == 2){ try {
            for (Sector objSector : Sector.getLista_sectores()) {
                if (objSector.getCod_sector().equals(codSector) && objSector.getCodigo_condominio().equals(codCondominio)) {
                    objSector.setNombre_sector(nombresector);
                    objSector.setCant_dpto(cantDepartamentos);
                }
            }
        } catch (ExcepcionNoEncuentraValorBuscado ex) {
            System.out.println(ex.getMessage());
        }

        } if(tipoRegistro == 1){
            Sector Dx = new Sector(codCondominio,codSector,nombresector,cantDepartamentos);
        }


        System.out.print("¿Desea registrar otro Sector? (Sí:1 || No:0):");
        seguirRegistro  = sc2.nextInt();

        if(seguirRegistro==1){ crear_ActualizarSector();
        }

    }


    public void crear_ActualizarDpto(){
        int seguirRegistro = 1;
        Scanner sc = new Scanner(System.in);
        System.out.println("========= CREAR/ACTUALIZAR DEPARTAMENTO==================================================");
        System.out.print("Tipo de registro (1: Crear | 2:Actualizar ):");
        int tipoRegistro = sc.nextInt();

        Scanner sc10 = new Scanner(System.in);
        System.out.print("Codigo de condominio:");
        String codCondominio = sc10.nextLine();

        Scanner sc11 = new Scanner(System.in);
        System.out.print("Sector:");
        String sector = sc11.nextLine();

       // Scanner sc12 = new Scanner(System.in);
        // String sector = sc12.nextLine();

        Scanner sc13 = new Scanner(System.in);
        System.out.print("Numero de dpto:");
        String numero_dpto = sc13.nextLine();

        Scanner sc14 = new Scanner(System.in);
        System.out.print("Area:");
        double area_dpto = sc14.nextDouble();

        Scanner sc2 = new Scanner(System.in);
        System.out.print("Propietario:");
        String propietario_dpto = sc2.nextLine();

        if(tipoRegistro == 2){ try {
            for (Departamento objDpto : Departamento.getDpto_sector()) {
                if (objDpto.getSector().equals(sector) && objDpto.getNumero_dpto().equals(numero_dpto)) {
                    objDpto.setArea_dpto(area_dpto);
                    objDpto.setHabitante_dpto(propietario_dpto);
                }
            }
        } catch (ExcepcionNoEncuentraValorBuscado ex) {
            System.out.println(ex.getMessage());
        }

        } if(tipoRegistro == 1){
            Departamento Dx = new Departamento(codCondominio, sector, numero_dpto, area_dpto, propietario_dpto);
        }


        System.out.print("¿Desea registrar otro departamento? (Sí:1 || No:0):");
        seguirRegistro  = sc2.nextInt();

        if(seguirRegistro==1){ crear_ActualizarDpto();
        }
    }

    public void crear_ActualizarCochera(){
        int seguirRegistro = 1;
        Scanner sc = new Scanner(System.in);
        System.out.println("========= CREAR/ACTUALIZAR COCHERA==================================================");
        System.out.print("Tipo de registro (1: Crear | 2:Actualizar ):");
        int tipoRegistro = sc.nextInt();

        Scanner sc10 = new Scanner(System.in);
        System.out.print("Codigo de condominio:");
        String codCondominio = sc10.nextLine();

        Scanner sc11 = new Scanner(System.in);
        System.out.print("Sector:");
        String sector = sc11.nextLine();

        Scanner sc13 = new Scanner(System.in);
        System.out.print("Numero de Cochera:");
        String numero_cochera = sc13.nextLine();

        Scanner sc14 = new Scanner(System.in);
        System.out.print("Propietario de Cochera:");
        String usuario_cochera = sc14.nextLine();

        Scanner sc3 = new Scanner(System.in);
        System.out.print("Número de documento:");
        String nrodpto = sc3.nextLine();

        String dptoRef = sector+ nrodpto;

        if(tipoRegistro == 2){ try {
            for (Cochera objCochera : Cochera.getCochera_sector()) {
                if (objCochera.getCochera_sector().equals(dptoRef) && objCochera.getNumero_cochera().equals(numero_cochera)) {
                    objCochera.setUsuario_cochera(usuario_cochera);
                    objCochera.setDpto_ref(dptoRef);
                }
            }
        } catch (ExcepcionNoEncuentraValorBuscado ex) {
            System.out.println(ex.getMessage());
        }

        } if(tipoRegistro == 1){
            Cochera Cx = new Cochera(codCondominio, sector, numero_cochera,usuario_cochera,dptoRef);
            //System.out.println(Cx.toString()); solo para prueba
        }

        Scanner sc2 = new Scanner(System.in);
        System.out.print("¿Desea registrar otra cochera? (Sí:1 || No:0):");
        seguirRegistro  = sc2.nextInt();

        if(seguirRegistro==1){ crear_ActualizarCochera();
        }



    }

    /*
    public void registrarCochera() {
        Scanner sc = new Scanner(System.in);
        System.out.println("========= INGRESAR COCHERA==================================================");
        System.out.print("Codigo de condominio:");
        String codCondominio = sc.nextLine();
        System.out.print("Sector:");
        String sector = sc.nextLine();
        System.out.print("Numero de cochera:");
        String numero_cochera = sc.nextLine();
        Scanner sc2 = new Scanner(System.in);
        System.out.print("Documento del propietario:");
        String propietario_cochera = sc2.nextLine();

        Cochera Cx = new Cochera(codCondominio, sector, numero_cochera,propietario_cochera);

    }

     */

    public void registrarAreaComun() {
        int seguirRegistro = 1;
        Scanner sc = new Scanner(System.in);
        System.out.println("========= INGRESAR ÁREA COMÚN==================================================");
        System.out.print("Codigo de condominio:");
        String codCondominio = sc.nextLine();
        System.out.print("Sector:");
        String sector = sc.nextLine();
        Scanner sc2 = new Scanner(System.in);
        System.out.print("Nombre de Area Común:");
        String nombre_areacomun = sc2.nextLine();

        AreaComun Ax = new AreaComun(codCondominio, sector, nombre_areacomun);
        System.out.print("¿Desea registrar otra area comun? (Sí:1 || No:0):");
        seguirRegistro = sc2.nextInt();

        if (seguirRegistro == 1) {
            crear_ActualizarCochera();
        }
    }

    public void mostrarDepartamentos() {
        Condominio Condom_c001 = new Condominio("C");
        Condom_c001.informacionGeneral();
        System.out.println(Departamento.getDpto_sector());
    }

   public void mostrarCocheras() {
        System.out.println(Departamento.getDpto_sector());
    }

    public void mostrarZonasComunes() {
        System.out.println(Cochera.getCochera_sector());
    }

    public void reporteGeneral(){
        int seguirRegistro = 1;
        String ruta_archivo1 = "C:/Users/frank/IdeaProjects/gestion-condominios/src/Borradores/DatosDpto.csv";
        String[][] tablaDpto = new String[0][];
        try {
            tablaDpto = CargarDpto.cargar_array_csv(ruta_archivo1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Scanner sc2 = new Scanner(System.in);
        System.out.println(Arrays.deepToString(tablaDpto));
        System.out.print("¿Desea registrar otra carga? (Sí:1 || No:0):");
        seguirRegistro = sc2.nextInt();

        if (seguirRegistro == 1) {
            reporteGeneral();
        }

    }
}
