package Programa.ConfigCondominio;

import Programa.Excepciones.ExcepcionNoEncuentraDocumento;
import Programa.Excepciones.ExcepcionRecibosEmitidos;
import Programa.FormatoVentanas.AppIngreso;

import java.util.Scanner;


public class AppGastoCondominio {

    public void mostrarMenuGastosCondominio() {
        AppIngreso ingreso1 = new AppIngreso();

        System.out.println("_____________________________________________________________________________________________________________________________________________________________________|");
        System.out.println("GESTIÓN DE GASTOS POR DEPARTAMENTO                                                                                                                                                |");
        System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------|");
        System.out.println("Por favor elija la acción a utilizar:");
        System.out.println("0: IR AL MENÚ PRINCIPAL");
        System.out.println("1: EMITIR RECIBOS PARA UN PERIODO");
        System.out.println("2: BUSCAR RECIBOS DE UN PERIODO");
        System.out.println("3: CAMBIAR ESTADO DE RECIBOS DE UN PERIODO");

        Scanner sc2 = new Scanner(System.in);
        System.out.print(">>>>Ingrese la opción elegida: ");
        int opcionSubModulo  = sc2.nextInt();


        if(opcionSubModulo == 0){
            ingreso1.mostrarMenu2();
        }if(opcionSubModulo == 1){
            emitirRecibos();
            mostrarMenuGastosCondominio();
        }if(opcionSubModulo == 2){
            buscaRecibo();
            mostrarMenuGastosCondominio();
        } if(opcionSubModulo == 3){
            ActualizarEstadoRecibo();
            mostrarMenuGastosCondominio();
        }

    }


    public void emitirRecibos() {
        // **************************************************************************************
        // *********  EMITIR RECIBOS PARA UN PERIODO --  NO PERMITE EMITIR PERIODOS YA GENERADOS

        int seguir = 1;

        Scanner sc = new Scanner(System.in);
        System.out.println("========= EMISIÓN DE RECIBOS POR PERIODO==============================================");
        System.out.print("Periodo:");
        String codCondominio = sc.nextLine();

        try {
            GastoDepartamento.emitirRecibos(codCondominio);
        } catch (ExcepcionRecibosEmitidos e) {
            System.out.println(e.getMessage());
        }

        Scanner sc2 = new Scanner(System.in);
        System.out.print("¿Desea emitir por otro periodo? (Sí:1 || No:0) : ");
        seguir  = sc2.nextInt();

        if(seguir==1){ emitirRecibos();
        }

    }


    public void buscaRecibo(){
        // ********* BUSCAR RECIBOS DE UN PERIODO  *********************
            int seguir = 1;

            System.out.println("========= BUSCAR RECIBOS POR PERIODO==============================================");

            Scanner sc10 = new Scanner(System.in);
            System.out.print("Código de Condominio:");
            String codCondominio = sc10.nextLine();

            Scanner sc11 = new Scanner(System.in);
            System.out.print("Código de Sector:");
            String codSector = sc11.nextLine();

            Scanner sc13 = new Scanner(System.in);
            System.out.print("Numero de Departamento:");
            String numDepartamento = sc13.nextLine();


            try {
            GastoDepartamento.buscarRecibo(codCondominio,codSector,numDepartamento);
        }
        catch (ExcepcionNoEncuentraDocumento e) {
            System.out.println(e.getMessage());
        }

        Scanner sc2 = new Scanner(System.in);
        System.out.print("¿Desea buscar otro documento? (Sí:1 || No:0) : ");
        seguir  = sc2.nextInt();

        if(seguir==1){ buscaRecibo();
        }
    }


    public void ActualizarEstadoRecibo() {
        int seguir = 1;

        System.out.println("========= ACTUALIZAR ESTADO DE RECIBOS==============================================");

        Scanner sc10 = new Scanner(System.in);
        System.out.print("Periodo:");
        String periodo = sc10.nextLine();

        Scanner sc11 = new Scanner(System.in);
        System.out.print("Código de Sector:");
        String codSector = sc11.nextLine();

        Scanner sc13 = new Scanner(System.in);
        System.out.print("Numero de Departamento:");
        String numDepartamento = sc13.nextLine();

        Scanner sc14 = new Scanner(System.in);
        System.out.print("Estado:");
        String estado = sc13.nextLine();


        // ********* CAMBIAR ESTADO DE RECIBOS DE UN PERIODO  *********************
        try {
            GastoDepartamento.cambiarEstadoRecibo(periodo,codSector,numDepartamento,estado);
        }
        catch (ExcepcionNoEncuentraDocumento e) {
            System.out.println(e.getMessage());
        }
        if (seguir == 1) {
            ActualizarEstadoRecibo();
        }

        Scanner sc2 = new Scanner(System.in);
        System.out.print("¿Desea actualizar otro documento? (Sí:1 || No:0) : ");
        seguir  = sc2.nextInt();

        if(seguir==1){ ActualizarEstadoRecibo();
        }

    }

}

