package Programa.ConfigCondominio;

public class Condominio {
    private String codigo_condominio;
    private String nombre_condominio;
    private String direccion_condominio;
    private double area_factor;
    //private ArrayList <Departamento> lista_base;



    public Condominio(String codigo_condominio, String nombre_condominio, String direccion_condominio) {
        this.codigo_condominio = codigo_condominio;
        this.nombre_condominio = nombre_condominio;
        this.direccion_condominio = direccion_condominio;
        //this.lista_base= new ArrayList<>();
    }

    public Condominio(String codigo_condominio) {
        this.codigo_condominio = codigo_condominio;
    }


    public String getCodigo_condominio() {
        return codigo_condominio;
    }

    public void setCodigo_condominio(String codigo_condominio) {
        this.codigo_condominio = codigo_condominio;
    }

    public String getNombre_condominio() {
        return nombre_condominio;
    }

    public void setNombre_condominio(String nombre_condominio) {
        this.nombre_condominio = nombre_condominio;
    }

    public String getDireccion_condominio() {
        return direccion_condominio;
    }

    public double getArea_factor() {return area_factor; }

    public void setArea_factor(double area_factor) { this.area_factor = area_factor; }

    public void setDireccion_condominio(String direccion_condominio) {
        this.direccion_condominio = direccion_condominio;
    }

    // metodo para determinar cocheras por departamento

    public void informacionGeneral(){
        String dpto ;
        int cantcochera;
        double areacocheras;
        String cocheras = null;
        double areafactor = 0.0;
        for (Departamento objDpto : Departamento.getDpto_sector()) {
            dpto = "" + objDpto.getSector() + objDpto.getNumero_dpto();
            cantcochera = 0;
            areacocheras = 0.0;
            cocheras = "|";

            for (Cochera objCochera : Cochera.getCochera_sector()) {
                if (objCochera.getDpto_ref().equals(dpto)) {
                    cantcochera += 1;
                    areacocheras += objCochera.getArea_cochera();
                    cocheras += objCochera.getNumero_cochera() + "|";
                }
            }
            objDpto.setCant_cocheras(cantcochera);
            objDpto.setArea_cocheras(areacocheras);
            objDpto.setDetalle((cocheras.equals("|")?"NA":cocheras));
            objDpto.setArea_total(objDpto.getArea_dpto()+ objDpto.getArea_cocheras());
            areafactor=areafactor+objDpto.getArea_dpto()+ objDpto.getArea_cocheras();
        }   setArea_factor(areafactor);
        for (Departamento objDpto : Departamento.getDpto_sector()){
            objDpto.setFactor((Math.round((objDpto.getArea_total()/areafactor)*100)/100.0));
        }

    }






    /*
    public double getArea_cochera() {
        return area_cochera;
    }

    public void setArea_cochera(double area_cochera) {
        this.area_cochera = area_cochera;
    }
    public void setArea_condominio(String area_condominio) {
        this.area_condominio = area_condominio;
    }

    public String getArea_condominio() {
        return area_condominio;
    }

     */
}
