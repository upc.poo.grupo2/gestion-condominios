package Programa.ConfigCondominio;

import java.util.ArrayList;

public class AreaComun extends Condominio {
    private String sector;
    private String nombre_areacomun;
    private static double area_areacomun = 0.0;
    private static String propietario_areacomun = "Condominio";
    private static ArrayList<AreaComun> zonas_comunes = new ArrayList<>();;

    public AreaComun(String codigo_condominio, String sector, String nombre_areacomun) {
        super(codigo_condominio);
        this.sector = sector;
        this.nombre_areacomun = nombre_areacomun;
        this.area_areacomun = area_areacomun;
        this.propietario_areacomun = propietario_areacomun;
        zonas_comunes.add(this);
    }

    public static ArrayList<AreaComun> getZonas_comunes() {
        return zonas_comunes;
    }

    @Override
    public String toString() {
        return "AreaComun {" +
                "sector='" + sector + '\'' +
                ", nombre='" + nombre_areacomun + '\'' +
                ", area='" + area_areacomun + '\'' +
                ", propietario='" + propietario_areacomun + '\'' +
                '}' + '\n';
    }
}
