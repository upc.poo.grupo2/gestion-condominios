package Programa.ConfigCondominio;

import java.util.ArrayList;

// https://stackoverflow.com/questions/16119102/automatically-adding-every-class-object-constructed-into-an-arraylist
// agregar todas las instancias a un Arraylist en la misma clase

public class Cochera extends Condominio {
    private String sector;
    private String numero_cochera;
    private double area_cochera ;
    private String propietario_cochera;
    private String usuario_cochera;
    private String dpto_ref;
    private static ArrayList<Cochera> cochera_sector = new ArrayList<>() ;

    public Cochera(String codigo_condominio, String sector, String numero_cochera,  String propietario_cochera) {
        super(codigo_condominio);
        this.sector = sector;
        this.numero_cochera = numero_cochera;
        this.area_cochera =  12.0;
        this.propietario_cochera = propietario_cochera;
        cochera_sector.add(this);
    }

    public Cochera(String codigo_condominio, String sector, String numero_cochera,  String propietario_cochera, String dpto_ref) {
        super(codigo_condominio);
        this.sector = sector;
        this.numero_cochera = numero_cochera;
        this.area_cochera =  12.0;
        this.propietario_cochera = propietario_cochera;
        this.dpto_ref = dpto_ref;
        cochera_sector.add(this);
    }

    public static ArrayList<Cochera> getCochera_sector() {
        return cochera_sector;
    }

    public void setUsuario_cochera(String usuario_cochera) {
        this.usuario_cochera = usuario_cochera;
    }

    public void setDpto_ref(String dpto_ref) {
        this.dpto_ref = dpto_ref;
    }

    public String getNumero_cochera() {
        return numero_cochera;
    }

    public String getUsuario_cochera() {
        return usuario_cochera;
    }

    public String getPropietario_cochera() {
        return propietario_cochera;
    }

    public double getArea_cochera() {
        return area_cochera;
    }

    public String getDpto_ref() {
        return dpto_ref;
    }

    @Override
    public String toString() {
        return "Cochera {" +
                "sector='" + sector + '\'' +
                ", numero='" + numero_cochera + '\'' +
                ", area=" + area_cochera +
                ", propietario='" + propietario_cochera + '\'' +
                ", usuario='" + (usuario_cochera==null?"NA":usuario_cochera) + '\'' +
                ", referencia='" + (dpto_ref==null?"NA":dpto_ref) + '\'' +
                '}' + '\n';
    }
}
