package Programa.FormatoVentanas;

import Programa.ConfigCondominio.AppConfigCondominio;
import Programa.ConfigCondominio.AppGastoCondominio;
import Programa.ConfigGastos.AppGastos;

import java.util.Scanner;

public class AppIngreso {

    public void mostrarMenu0() {
        System.out.println("|=======================================================================================================================|");
        System.out.println("|=======================================================================================================================|");
        System.out.println("| SGC: SISTEMA DE GESTIÓN DE CONDOMINIOS                                                                                |");
        System.out.println("|-----------------------------------------------------------------------------------------------------------------------|");
        System.out.println("|                                                                                                                       |");
        System.out.println("|MENÚ PRINCIPAL                                                                                                         |");
        System.out.println("|Por Favor elija el módulo a utilizar:                                                                                  |");
        System.out.println("| 1: CONFIGURACIÓN DE LOS CONDOMINIOS                                                                                   |");
        System.out.println("| 2: INGRESAR GASTOS DEL CONDOMINIO                                                                                                |");
        System.out.println("| 3: GESTIÓN DE GASTOS POR DEPARTAMENTO                                                                                                              |");
        System.out.println("| 4: CONFIGURACIÓN DE USUARIOS                                                                                          |");
        System.out.println("|                                                                                                                       |");

    }

    public void mostrarMenu2() {
        System.out.println("|=======================================================================================================================|");
        System.out.println("|=======================================================================================================================|");
        System.out.println("| SGC: SISTEMA DE GESTIÓN DE CONDOMINIOS                                                                                |");
        System.out.println("|-----------------------------------------------------------------------------------------------------------------------|");
        System.out.println("|                                                                                                                       |");
        System.out.println("|MENÚ PRINCIPAL                                                                                                         |");
        System.out.println("|Por Favor elija el módulo a utilizar:                                                                                  |");
        System.out.println("| 1: CONFIGURACIÓN DE LOS CONDOMINIOS                                                                                   |");
        System.out.println("| 2: INGRESAR GASTOS DEL CONDOMINIO                                                                                              |");
        System.out.println("| 3: GESTIÓN DE GASTOS POR DEPARTAMENTO                                                                                                             |");
        System.out.println("| 4: CONFIGURACIÓN DE USUARIOS                                                                                          |");
        System.out.println("|                                                                                                                       |");
        System.out.print("|  >>>>Ingrese el número de la opción elegida:");
        Scanner sc = new Scanner(System.in);
        int moduloElegido = sc.nextInt();

        if(moduloElegido == 1){
            AppConfigCondominio appConfigCondominio = new AppConfigCondominio();
            appConfigCondominio.mostrarMenuAppConfigCondominio();
        } if(moduloElegido == 2){
            AppGastos appGastos = new AppGastos();
            appGastos.MostrarMenuGastos();
        } if(moduloElegido == 3){
            AppGastoCondominio appGastoCondominio=new AppGastoCondominio();
            appGastoCondominio.mostrarMenuGastosCondominio();
        } if(moduloElegido == 4){
            // modulo 4
        }

    }
    }


