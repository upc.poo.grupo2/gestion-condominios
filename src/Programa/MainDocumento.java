package Programa;

import Programa.ConfigGastos.Documento;
import Programa.ConfigGastos.GastoComun;
import Programa.Excepciones.ExcepcionDocumentoduplicado;

public class MainDocumento {
    public static void main(String[] args) {
        GastoComun gastoComun=new GastoComun();

        try {
            gastoComun.RegistrarDocumento(new Documento("mantto", "001","2109",22092011, 30092011,2010900770, "CCU sac", "c", "gasto1", 1.0, 50.0, "22/09/2021"));
            gastoComun.RegistrarDocumento(new Documento("luz", "002", "2109",30082011,30092011, 2010900770, "CCU sac", "comun", "gasto2", 1.0, 75.0, "22/09/2021"));
            gastoComun.RegistrarDocumento(new Documento("luz", "003","2109", 15082011, 30092011,2035221443, "CCU sac", "a", "gasto3", 1.0, 100.0, "22/09/2021"));
            gastoComun.RegistrarDocumento(new Documento("servicio", "142","2109", 15082011, 30092011,2035221443, "CCU sac", "comun", "Jardineria", 4.0, 130.0, "23/09/2021"));
            gastoComun.RegistrarDocumento(new Documento("luz", "004","2109", 15082011, 30092011,2035221410, "CCU sac", "b", "gasto4", 1.0, 130.0, "23/09/2021"));
            gastoComun.RegistrarDocumento(new Documento("agua", "005","2109", 15082011, 30092011,2035221410, "CCU sac", "b", "gasto5", 1.0, 45.0, "23/09/2021"));
            gastoComun.RegistrarDocumento(new Documento("agua", "006","2109", 15082011, 30092011,2035221411, "CCU sac", "a", "gasto6", 1.0, 90.0, "23/09/2021"));
            gastoComun.RegistrarDocumento(new Documento("servicio", "007","2109", 15082011, 30092011,2035221411, "CCU sac", "a", "gasto7", 1.0, 23.0, "23/09/2021"));
            gastoComun.RegistrarDocumento(new Documento("servicio", "008","2109", 15082011, 30092011,2035221412, "CCU sac", "c", "gasto8", 1.0, 32.0, "23/09/2021"));

            //dato de excepcion
             gastoComun.RegistrarDocumento(new Documento("servicio", "001","2109", 10072011, 30092011,2010900770, "CCU sac", "C", "Jardinerai", 2.0, 125.0, "22/09/2021"));

        }catch (ExcepcionDocumentoduplicado e){
            System.out.println(e.getMessage());
        }
        System.out.println("lista");
        gastoComun.listDocumentos();
        System.out.println("busqueda tipo");
        System.out.println(gastoComun.BusquedaTipo("servicio"));
        System.out.println("busqueda torre");
        System.out.println(gastoComun.Busquedaxtorre("c"));
        System.out.println("**** busqueda estado");
        System.out.println(gastoComun.BusquedaxEstado("no"));
        System.out.println("**** cambio estado");
        gastoComun.CambioEstado("si","003",2035221443);
        System.out.println(gastoComun.Busquedaxndocumento("003"));
        System.out.println("**** calculototalxtorre");
        System.out.println(gastoComun.TotalxPeriodoxtorre("2109","c"));
        System.out.println(gastoComun.TotalxPeriodo("2109"));
        System.out.println("**** calculototalxtorrexcancelado");
        System.out.println(gastoComun.TotalxPeriodoxtorrexCancelado(9,21,"C","no"));

        System.out.println("**** busqueda por periodo");
        System.out.println(gastoComun.Busquedaxperiodo(9,21 ));

        System.out.println("**** busqueda por periodo cancelado");
        System.out.println(gastoComun.Busquedaxperiodoxcancelado(8,21 ,"si"));

        System.out.println(GastoComun.TotalxPeriodoxtorre("2109","comun"));



//        System.out.println("**** busqueda estado");
//        System.out.println(gastoComun.BusquedaxEstado("no"));
            System.out.println("****");
            gastoComun.listDocumentos();
//        System.out.println("****");
//
//        System.out.println(gastoComun.BusquedaxEstado("si"));


    }
}
