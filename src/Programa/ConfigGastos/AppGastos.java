package Programa.ConfigGastos;

import Programa.ConfigCondominio.GastoDepartamento;
import Programa.Excepciones.ExcepcionDocumentoduplicado;
import Programa.FormatoVentanas.AppIngreso;

import java.util.ArrayList;
import java.util.Scanner;

public class AppGastos {

    public static int entradaTeclado;
    public static Scanner entradaEscaner = new Scanner (System.in);
    GastoComun gastoComun=new GastoComun();


    public static void main(String[] args) {

       // MostrarMenuGastos();


    }

    ///////Menu////////////////////////////////////////////////////////////////////////////////////
    public void MostrarMenuGastos(){
        AppIngreso ingreso1 = new AppIngreso();

        String tecla;
        do {
            System.out.println("|=======================================================================================================================|");
            System.out.println("|=======================================================================================================================|");
            System.out.println("|     MODULO DE GASTOS COMUNES                                                                                          |");
            System.out.println("|-----------------------------------------------------------------------------------------------------------------------|");
            System.out.println("|     ELEGIR UNA OPCION                                                                                                 |");
            System.out.println("|                                                                                                                       |");
            System.out.println("| 1. INGRESAR GASTOS COMUNES                                                                                            |");
            System.out.println("| 2. MODIFICAR GASTOS COMUNES                                                                                           |");
            System.out.println("| 3. BUSCAR GASTOS COMUNES                                                                                              |");
            System.out.println("| 4. REGRESAR AL MENU PRINCIPAL                                                                                         |");
            System.out.println("|                                                                                                                       |");
            System.out.println("|-----------------------------------------------------------------------------------------------------------------------|");

            entradaTeclado = entradaEscaner.nextInt();
            boolean opc = (entradaTeclado <= 5);
            if (opc) {
                switch (entradaTeclado) {
                    case (1) -> Ingresogastos();
                    case (2) -> Modificargastos();
                    case (3) -> Buscargastos();
                    case (4) -> ingreso1.mostrarMenu2();
                    default -> System.out.println("Debe elegir una opción del 1 al 4");
                }
            }
            System.out.print("\n¿Quiere seguir? (S)Sí ó (N)No\n");
            tecla = new Scanner(System.in).nextLine();
        } while (tecla.equals("s") || tecla.equals("S"));
    }




    ///////Ingreso Gastos////////////////////////////////////////////////////////////////////////////////////
    public void Ingresogastos(){


        String tecla;
        do {
            System.out.println("|=======================================================================================================================|");
            System.out.println("|=======================================================================================================================|");
            System.out.println("|     INGRESAR GASTOS COMUNES                                                                                           |");
            System.out.println("|-----------------------------------------------------------------------------------------------------------------------|");


        System.out.print("ingrese numero de documento: ");
        Scanner entradadoc = new Scanner (System.in);
        String numerodocumento = entradadoc.nextLine ();
        System.out.print("ingrese el tipo: ");
        Scanner entradatipo = new Scanner (System.in);
        String tipo = entradatipo.nextLine ();
        System.out.print("ingrese el area comun: ");
        Scanner entradatorre = new Scanner (System.in);
        String torre = entradatorre.nextLine ();
        System.out.print("ingrese periodo del documento (añomes : AAMM ): ");
        Scanner entradaperiodo = new Scanner (System.in);
        String periodo = entradaperiodo.nextLine ();
        System.out.print("ingrese fecha de emision del documento: ");
        Scanner entradafechadoc = new Scanner (System.in);
        int fechadoc = entradafechadoc.nextInt ();
        System.out.print("ingrese fecha de vencimiento del documento: ");
        Scanner entradafechavenc = new Scanner (System.in);
        int fechavenc = entradafechavenc.nextInt ();
        System.out.print("ingrese fecha el ruc del documento: ");
        Scanner entradaruc = new Scanner (System.in);
        int ruc = entradaruc.nextInt ();
        System.out.print("ingrese el nombre de proveedor: ");
        Scanner entradanombre = new Scanner (System.in);
        String nombre = entradanombre.nextLine ();
        System.out.print("ingrese el fecha de ingreso: ");
        Scanner entradafechaingreso = new Scanner (System.in);
        String fechaingreso = entradafechaingreso.nextLine ();
        System.out.println("ingrese Detalle de compra");
        System.out.print("ingrese la descripcion del productor: ");
        Scanner entradaproducto = new Scanner (System.in);
        String producto = entradaproducto.nextLine ();
        System.out.print("ingrese la cantidad del productor: ");
        Scanner entradacantidad = new Scanner (System.in);
        Double cantidad = entradacantidad.nextDouble ();
        System.out.println("ingrese la precio del productor: ");
        Scanner entradaprecio = new Scanner (System.in);
        Double precio = entradaprecio.nextDouble ();

        try {
           gastoComun.RegistrarDocumento(new Documento(tipo, numerodocumento,periodo,fechadoc, fechavenc,ruc
                   , nombre, torre, producto, cantidad, precio, fechaingreso));

           //gastoComun.RegistrarDocumento(new Documento("electricidad", "002", 921,30082011,30092011, 2010900770, "CCU sac", "C", "Jardineria", 1.0, 125.0, "22/09/2021"));
          // gastoComun.RegistrarDocumento(new Documento("servicio", "003",921, 15082011, 30092011,2035221443, "CCU sac", "C", "Jardineria", 4.0, 125.0, "22/09/2021"));
         }catch (ExcepcionDocumentoduplicado e) {
            System.out.println(e.getMessage());
        }
            System.out.print("\n¿Quiere ingresar otro documento? (S)Sí ó (N)No, regresar al menu Gastos Comunes\n");
           tecla = new Scanner(System.in).nextLine();
        } while (tecla.equals("s") || tecla.equals("S"));
        if(tecla.equals("n")||tecla.equals("N")){
            MostrarMenuGastos();
        }
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void Modificargastos(){
        String tecla;
        String titulo;
        GastoComun gastoComun=new GastoComun();
        do {
            System.out.println("|=======================================================================================================================|");
            System.out.println("|=======================================================================================================================|");
            System.out.println("|     MODIFICAR DOCUMENTOS DE GASTOS COMUNES                                                                            |");
            System.out.println("|-----------------------------------------------------------------------------------------------------------------------|");
            System.out.println("|            ELEGIR UNA OPCION                                                                                          |");
            System.out.println("| 1. MODIFICAR NUMERO DE DOCUMENTO                                                                                      |");
            System.out.println("| 2. MODIFICAR TIPO DE DOCUMENTO                                                                                        |");
            System.out.println("| 3. MODIFICAR TORRE DE DOCUMENTO                                                                                       |");
            System.out.println("| 4. MODIFICAR RUC DE DOCUMENTO                                                                                         |");
            System.out.println("| 5. MODIFICAR PERIODO DE DOCUMENTO                                                                                     |");
            System.out.println("|-----------------------------------------------------------------------------------------------------------------------|");
            System.out.println("| 6. REGRESAR AL MENU DE GASTOS COMUNES                                                                                 |");
            System.out.println("|                                                                                                                       |");
            System.out.println("|---- Elija un módulo -----                                                                                             |");

            entradaTeclado = entradaEscaner.nextInt();
            int opc = entradaTeclado;
            if(opc==1){
                 titulo=" Modificar Numero de Documento";
            }else if(opc==2){
                 titulo=" Modificar Tipo de Documento";
            }else if(opc==3){
                 titulo=" Modificar Torre de Documento";
            }else if(opc==4){
                 titulo=" Modificar Ruc de Documento";
            }else{
                 titulo=" Modificar Periodo de Documento";
            }

            if (opc>=1 && opc<=5) {

                System.out.println(titulo);
                System.out.println("|-----------------------------------------------------------------------------------------------------------------------|");
                System.out.println("ingrese numero de documento que se va cambiar");
                Scanner entradadoc = new Scanner (System.in);
                String numerodocumento = entradadoc.nextLine ();
                System.out.println("ingrese numero de ruc");
                Scanner entradaruc = new Scanner (System.in);
                int numeroruc = entradaruc.nextInt ();

                switch (entradaTeclado) {
                    case (1) -> {
                        System.out.println("ingrese el nuevo numero del documento "+numerodocumento +" :");
                        Scanner entradanuevodoc = new Scanner(System.in);
                        String numeronuevodocumento = entradanuevodoc.nextLine();
                        gastoComun.Cambionumdoc(numeronuevodocumento, numerodocumento, numeroruc);
                    }
                    case (2) -> {
                        System.out.println("ingrese el nuevo de tipo del documento "+numerodocumento +" :");
                        Scanner entradanuevotipo = new Scanner(System.in);
                        String numeronuevotipo = entradanuevotipo.nextLine();
                        gastoComun.Cambiotipo(numeronuevotipo, numerodocumento, numeroruc);
                    }
                    case (3) -> {
                        System.out.println("ingrese la nuevo torre del documento "+numerodocumento +" :");
                        Scanner entradanuevotorre = new Scanner(System.in);
                        String numeronuevotorre = entradanuevotorre.nextLine();
                        gastoComun.CambioTorre(numeronuevotorre, numerodocumento, numeroruc);
                    }
                    case (4) -> {
                        System.out.println("ingrese el nuevo ruc del documento "+numerodocumento +" :");
                        Scanner entradanuevoruc = new Scanner(System.in);
                        int numeronuevoruc = entradanuevoruc.nextInt();
                        gastoComun.CambioRuc(numeronuevoruc, numerodocumento, numeroruc);
                    }
                    case (5) -> {
                        System.out.println("ingrese el nuevo Periodo del documento "+numerodocumento +" :");
                        Scanner entradanuevoperiodo = new Scanner(System.in);
                        String numeronuevoperiodo = entradanuevoperiodo.nextLine();
                        gastoComun.CambioPeriodo(numeronuevoperiodo, numerodocumento, numeroruc);
                    }
                }
            }
            System.out.print("\n¿Quiere realizar otra modificacion? (S)Sí ó No, regresar al menu Gastos Comunes (N)\n");
            tecla = new Scanner(System.in).nextLine();
        } while (tecla.equals("s") || tecla.equals("S"));
        if(tecla.equals("n")||tecla.equals("N")){
            MostrarMenuGastos();
        }
    }




    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void Buscargastos() {

        try {
            gastoComun.RegistrarDocumento(new Documento("mantto", "001","2109",22092011, 30092011,2010900770, "CCU sac", "c", "gasto1", 1.0, 50.0, "22/09/2021"));
            gastoComun.RegistrarDocumento(new Documento("luz", "002", "2109",30082011,30092011, 2010900770, "CCU sac", "comun", "gasto2", 1.0, 75.0, "22/09/2021"));
            gastoComun.RegistrarDocumento(new Documento("luz", "003","2109", 15082011, 30092011,2035221443, "CCU sac", "a", "gasto3", 1.0, 100.0, "22/09/2021"));
            gastoComun.RegistrarDocumento(new Documento("servicio", "142","2109", 15082011, 30092011,2035221443, "CCU sac", "comun", "Jardineria", 4.0, 130.0, "23/09/2021"));
            gastoComun.RegistrarDocumento(new Documento("luz", "004","2109", 15082011, 30092011,2035221410, "CCU sac", "b", "gasto4", 1.0, 130.0, "23/09/2021"));
            gastoComun.RegistrarDocumento(new Documento("agua", "005","2109", 15082011, 30092011,2035221410, "CCU sac", "b", "gasto5", 1.0, 45.0, "23/09/2021"));
            gastoComun.RegistrarDocumento(new Documento("agua", "006","2109", 15082011, 30092011,2035221411, "CCU sac", "a", "gasto6", 1.0, 90.0, "23/09/2021"));
            gastoComun.RegistrarDocumento(new Documento("servicio", "007","2109", 15082011, 30092011,2035221411, "CCU sac", "a", "gasto7", 1.0, 23.0, "23/09/2021"));
            gastoComun.RegistrarDocumento(new Documento("servicio", "008","2109", 15082011, 30092011,2035221412, "CCU sac", "c", "gasto8", 1.0, 32.0, "23/09/2021"));

            //dato de excepcion
            gastoComun.RegistrarDocumento(new Documento("servicio", "001","2109", 10072011, 30092011,2010900770, "CCU sac", "C", "Jardinerai", 2.0, 125.0, "22/09/2021"));

        }catch (ExcepcionDocumentoduplicado e){
            System.out.println(e.getMessage());
        }

        String tecla;
        String titulo ;
        do {
            System.out.println("|=======================================================================================================================|");
            System.out.println("|=======================================================================================================================|");
            System.out.println("|     BUSCAR DOCUMENTOS DE GASTOS COMUNES                                                                               |");
            System.out.println("|-----------------------------------------------------------------------------------------------------------------------|");
            System.out.println("| 1.  Buscar Todos los Gastos Comunes                                                                                   |");
            System.out.println("| 2.  Buscar Gastos por Torre                                                                                           |");//
            System.out.println("| 3.  Buscar Gastos por Tipo                                                                                            |");//
            System.out.println("| 4.  Buscar Gastos por Documentos                                                                                      |");//
            System.out.println("| 5.  Buscar Gastos por estado                                                                                          |");//
            System.out.println("| 6.  Buscar Gastos por Periodo                                                                                         |");//
            System.out.println("| 7.  Buscar Gastos por Ruc                                                                                             |");
            System.out.println("| 8.  Buscar Gastos por Torre - Tipo                                                                                    |");
            System.out.println("| 9.  Buscar Gastos por Torre - Estado                                                                                  |");
            System.out.println("| 10. Buscar Gastos por Periodo - Estado                                                                                |");//
            System.out.println("| 11. Buscar Gastos por Periodo - Torre                                                                                 |");//
            System.out.println("| 12. Buscar Gastos por Periodo - Torre - cancelado                                                                     |");
            System.out.println("|-----------------------------------------------------------------------------------------------------------------------|");
            System.out.println("| 13. Regresar al Menu Gastos Comunes                                                                                   |");
            System.out.println("|-----------------------------------------------------------------------------------------------------------------------|");
            System.out.println("|- Elija una opción ---                                                                                                 |");
            entradaTeclado = entradaEscaner.nextInt();

            int opc = entradaTeclado;
            if(opc==1){
                titulo=" Busqueda de Todos los Gastos Comunes";
            }else if(opc==2){
                titulo=" Busqueda de Gastos por Torre";
            }else if(opc==3){
                titulo=" Busqueda de Gastos por Tipo";
            }else if(opc==4){
                titulo=" Busqueda de Gastos por Documentos";
            }else if(opc==5){
                titulo=" Busqueda de Gastos por estado";
            }else if(opc==6){
                titulo=" Busqueda de Gastos por Periodo";
            }else if(opc==7){
                titulo=" Busqueda de Gastos por Ruc";
            }else if(opc==8){
                titulo=" Busqueda de Gastos por Torre - Tipo";
            }else if(opc==9){
                titulo=" Busqueda de Gastos por Torre - Estado";
            }else if(opc==10){
                titulo=" Busqueda de Gastos por Periodo - Estado";
            }else if(opc==11){
                titulo=" Busqueda de Gastos por Periodo - Torre";
            }else{
                titulo=" Busqueda de Gastos por Periodo - Torre - cancelado";
            }

            if (opc>=1 && opc<=12) {
                System.out.println(titulo);
                System.out.println(" ================================== ");
                switch (entradaTeclado) {
                    case (1) -> {
                        gastoComun.listDocumentos();
                    }
                    case (2) -> {
                        System.out.print("ingrese la torre a buscar: ");
                        Scanner entradatorre = new Scanner(System.in);
                        String torre = entradatorre.nextLine();
                        System.out.println(gastoComun.Busquedaxtorre(torre));
                    }
                    case (3) -> {
                        System.out.print("ingrese el tipo a buscar: ");
                        Scanner entradatipo = new Scanner(System.in);
                        String tipo = entradatipo.nextLine();
                        System.out.println(gastoComun.BusquedaTipo(tipo));
                    }
                    case (4) -> {
                        System.out.print("ingrese el Documento a buscar: ");
                        Scanner entradadocumento= new Scanner(System.in);
                        String documento = entradadocumento.nextLine();
                        System.out.println(gastoComun.Busquedaxndocumento(documento));
                    }
                    case (5) -> {
                        System.out.print("ingrese el Estado a buscar (si/no): ");
                        Scanner entradaestado= new Scanner(System.in);
                        String estado = entradaestado.nextLine();
                        System.out.println(gastoComun.BusquedaxEstado(estado));
                    }
                    case (6) -> {
                        System.out.print("ingrese el Mes a buscar: ");
                        Scanner entradaperiodomes= new Scanner(System.in);
                        int periodomes = entradaperiodomes.nextInt();
                        System.out.print("ingrese el Año a buscar: ");
                        Scanner entradaperiodoanio= new Scanner(System.in);
                        int periodoanio = entradaperiodoanio.nextInt();
                        System.out.println(gastoComun.Busquedaxperiodo(periodomes,periodoanio));
                    }
                    case (7) -> {
                        System.out.print("ingrese el Ruc a buscar: ");
                        Scanner entradaruc =new Scanner(System.in);
                        int ruc = entradaruc.nextInt();
                        System.out.println(gastoComun.Busquedaxruc(ruc));
                    }
                    case (8) -> {
                        System.out.print("ingrese el Torre a buscar: ");
                        Scanner entradatorre= new Scanner(System.in);
                        String torre = entradatorre.nextLine();
                        System.out.print("ingrese el Tipo a buscar: ");
                        Scanner entradatipo= new Scanner(System.in);
                        String tipo = entradatipo.nextLine();
                        System.out.println(gastoComun.Busquedaxtorrextipo(torre,tipo));
                    }
                    case (9) -> {
                        System.out.print("ingrese el Torre a buscar: ");
                        Scanner entradatorre= new Scanner(System.in);
                        String torre = entradatorre.nextLine();
                        System.out.print("ingrese el estado a buscar: (si/no)");
                        Scanner entradaestado= new Scanner(System.in);
                        String estado = entradaestado.nextLine();
                        System.out.println(gastoComun.Busquedaxtorrextipo(torre,estado));
                    }
                    case (10) -> {
                        System.out.print("ingrese el Mes a buscar: ");
                        Scanner entradaperiodomes= new Scanner(System.in);
                        int periodomes = entradaperiodomes.nextInt();
                        System.out.print("ingrese el Año a buscar: ");
                        Scanner entradaperiodoanio= new Scanner(System.in);
                        int periodoanio = entradaperiodoanio.nextInt();
                        System.out.print("ingrese el estado a buscar: (si/no)");
                        Scanner entradaestado= new Scanner(System.in);
                        String estado = entradaestado.nextLine();
                        System.out.println(gastoComun.Busquedaxperiodoxcancelado(periodomes,periodoanio,estado));
                    }
                    case (11) -> {
                        System.out.print("ingrese el Mes a buscar: ");
                        Scanner entradaperiodomes= new Scanner(System.in);
                        int periodomes = entradaperiodomes.nextInt();
                        System.out.print("ingrese el Año a buscar: ");
                        Scanner entradaperiodoanio= new Scanner(System.in);
                        int periodoanio = entradaperiodoanio.nextInt();
                        System.out.print("ingrese la torre a buscar:");
                        Scanner entradatorre= new Scanner(System.in);
                        String torre = entradatorre.nextLine();
                        System.out.println(gastoComun.Busquedaxperiodoxtorre(periodomes,periodoanio,torre));
                    }
                    case (12) -> {
                        System.out.print("ingrese el Mes a buscar: ");
                        Scanner entradaperiodomes= new Scanner(System.in);
                        int periodomes = entradaperiodomes.nextInt();
                        System.out.print("ingrese el Año a buscar: ");
                        Scanner entradaperiodoanio= new Scanner(System.in);
                        int periodoanio = entradaperiodoanio.nextInt();
                        System.out.print("ingrese la torre a buscar:");
                        Scanner entradatorre= new Scanner(System.in);
                        String torre = entradatorre.nextLine();
                        System.out.print("ingrese el estado a buscar: (si/no)");
                        Scanner entradaestado= new Scanner(System.in);
                        String estado = entradaestado.nextLine();
                        System.out.println(gastoComun.Busquedaxperiodoxtorrexcancelado(periodomes,periodoanio,torre,estado));
                    }
                    case (13) -> MostrarMenuGastos();
                    default -> System.out.println("Debe elegir una opción entre 1-12");
                }

            }
            System.out.print("\n¿Quieres realizar otra busqueda? (S)Sí ó No, regresar la menu Gastos Comunes (N)\n");
            tecla = new Scanner(System.in).nextLine();
        } while (tecla.equals("s") || tecla.equals("S"));
        if (tecla.equals("n") || tecla.equals("N")) {
            MostrarMenuGastos();
        }


    }



   ////fin de appgasto
}
