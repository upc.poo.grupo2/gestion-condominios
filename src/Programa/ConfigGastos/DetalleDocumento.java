package Programa.ConfigGastos;

public class DetalleDocumento {

    private String descripcion;
    private Double cantidad;
    private Double precio;
    private  Double subtotal;


    public DetalleDocumento(String descripcion, Double cantidad, Double precio) {

        this.descripcion = descripcion;
        this.cantidad = cantidad;
        this.precio = precio;
        this.subtotal = CalcSubtotal();

    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public Double getSubtotal() {
        return subtotal;
    }




    @Override
    public String toString() {
        return  "descripcion='" + descripcion + '\'' +
                ", cantidad=" + cantidad +
                ", precio=" + precio +
                ", subtotal=" + subtotal +
                '}';
    }

    public double CalcSubtotal(){
        return cantidad*precio;

    }
}
