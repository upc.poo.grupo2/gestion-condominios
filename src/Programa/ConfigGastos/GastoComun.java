package Programa.ConfigGastos;

import Programa.Excepciones.ExcepcionDocumentoduplicado;
import java.util.ArrayList;

public class GastoComun {
        private static ArrayList<Documento> documentos = new ArrayList<>(); // SE MODFICO A STATIC
        private ArrayList<Documento> listabuscadoestado;
        private ArrayList<Documento> listabusquedatipo;
        private ArrayList<Documento> listabuscadatorre;
        private ArrayList<Documento> listabuscadadoc;
        private ArrayList<Documento> listabuscadaruc;
        private ArrayList<Documento> listabuscadaperiodo; // agregado para la busqueda por periodo

        public GastoComun(){}// {this.documentos = new ArrayList<>();}
        //metodos
        ////=================REGISTRAR=================================
        //REGISTRAR DOCUMENTO                   | RegistrarDocumento(Documento documento)
        //LISTA LOS DOCUMENTOS                  | listDocumentos()
        ////=================BUSQUEDA================================

        //BUSQUEDA POR TIPO                     | BusquedaTipo(String tipo)
        //BUSQUEDA POR ESTADO(CANCELADO)        | BusquedaxEstado(String estado)
        //BUSQUEDA POR TORRE                    | Busquedaxtorre(String torre)
        //BUSQUEDA POR DOCUMENTO                | Busquedaxndocumento(String documento)
        //BUSQUEDA POR RUC                      | Busquedadocsxruc(int ruc)
        //BUSQUEDA POR TORRE Y TIPO             | Busquedaxtorrextipo(String torre,String tipo)
        //BUSQUEDA POR TORRE Y CANCELADO        | Busquedaxtorrexcancelado(String torre,String cancelado)
        //BUSQUEDA POR PERIODO                  | Busquedaxperiodo(int mes,int anio)
        //BUSQUEDA POR PERIODO Y CANCELADO      | Busquedaxperiodoxcancelado(int mes,int anio,String cancelado)
        //BUSQUEDA POR PERIODO Y TORRE          | Busquedaxperiodoxtorre(int mes,int anio,String torre)
        //BUSQUEDA POR PERIODO,TORRE Y CANCELADO| Busquedaxperiodoxtorrexcancelado(int mes,int anio,String torre,String cancelado)

        ////=================MODIFICAR=================================

        //CAMBIO DE TIPO                | CambioEstado(String estado,String doc,int ruc)
        //CAMBIO DE RUC                 | CambioRuc(String tipo,String doc,int ruc)
        //CAMBIO DE TORRE               | CambioTorre(String Torre,String doc,int ruc)
        //CAMBIO DE DOCUMENTO           | Cambionumdoc(String nuevodoc,String doc,int ruc)

        ////=================TOTALES=================================
        //TOTAL POR PERIODO                      | TotalxPeriodo(int mes,int anio)
        //TOTAL POR PERIODO Y TORRE              | TotalxPeriodoxtorre(int mes,int anio,String torre)
        //TOTAL POR PERIODO ,TORRE Y CANCELADO   |TotalxPeriodoxtorrexCancelado(int mes,int anio,String torre,String cancelado)



        public ArrayList<Documento> getDocumentos() {
                return documentos;
        }

        public void setDocumentos(ArrayList<Documento> documentos) {
                this.documentos = documentos;
        }

        public void RegistrarDocumento(Documento documento) throws ExcepcionDocumentoduplicado {
                boolean duplicado=false;
                for (Documento d:documentos){
                        if(documento.getNumeroDocumento().equals(d.getNumeroDocumento()) && documento.getRuc()==d.getRuc()){
                                throw new ExcepcionDocumentoduplicado("el numero "+d.getNumeroDocumento()+" de documento ya fue registrado para el ruc "+d.getRuc());
                        }
                }
                if(!duplicado) {
                        documentos.add(documento);

                }
        }

        public void listDocumentos(){
                for(Documento d:documentos){
                        System.out.print(d);
                }
        }
        //=====MODIFICACION DE DATOS=========

        public void CambioEstado(String estado,String doc,int ruc) {
                for(Documento d:documentos){
                        if(d.getNumeroDocumento().equals(doc) && d.getRuc()==ruc){
                               d.setCancelado(estado);
                                break;
                        }
                }
//
        }

        public void CambioRuc(int nuevoruc,String doc,int ruc) {
                for(Documento d:documentos){
                        if(d.getNumeroDocumento().equals(doc) && d.getRuc()==ruc){
                                d.setRuc(nuevoruc);
                                break;
                        }
                }
        }
        public void CambioTorre(String Torre,String doc,int ruc) {
                for(Documento d:documentos){
                        if(d.getNumeroDocumento().equals(doc) && d.getRuc()==ruc){
                                d.setTorre(Torre);
                                break;
                        }
                }
        }
        public void Cambionumdoc(String nuevodoc,String doc,int ruc) {
                for(Documento d:documentos){
                        if(d.getNumeroDocumento().equals(doc) && d.getRuc()==ruc){
                                d.setNumeroDocumento(nuevodoc);
                                break;
                        }
                }
        }
        public void Cambiotipo(String tipo,String doc,int ruc) {
                for(Documento d:documentos){
                        if(d.getNumeroDocumento().equals(doc) && d.getRuc()==ruc){
                                d.setTipo(tipo);
                                break;
                        }
                }
        }
        public void CambioPeriodo(String periodo,String doc,int ruc) {
                for(Documento d:documentos){
                        if(d.getNumeroDocumento().equals(doc) && d.getRuc()==ruc){
                                d.setPeriodo(periodo);
                                break;
                        }
                }
        }
        //===========================FIN MODIFICACION=================================

        //============================BUSQUEDAS=======================================
        public ArrayList<Documento> BusquedaTipo(String tipo) {
                listabusquedatipo=new ArrayList<>();
                for(Documento d:documentos){
                        if(tipo.equals(d.getTipo())){
                                listabusquedatipo.add(d);
                        }
                }
                return  listabusquedatipo;
        }


        public ArrayList<Documento> BusquedaxEstado(String estado) {
                listabuscadoestado=new ArrayList<>();
                for(Documento d:documentos){
                        if(estado.equals(d.getCancelado())){
                                listabuscadoestado.add(d);
                        }
                }
                return listabuscadoestado;
        }

        public ArrayList<Documento> Busquedaxtorre(String torre) {
                listabuscadatorre=new ArrayList<>();
                for(Documento d:documentos){
                        if(torre.equals(d.getTorre())){
                                listabuscadatorre.add(d);
                        }
                }
                return listabuscadatorre;
        }
        public ArrayList<Documento> Busquedaxndocumento(String documento) {
                listabuscadadoc=new ArrayList<>();
                for(Documento d:documentos){
                        if(documento.equals(d.getNumeroDocumento())){
                                listabuscadadoc.add(d);
                        }
                }
                return listabuscadadoc;
        }
        public ArrayList<Documento> Busquedaxruc(int ruc) {
                listabuscadaruc=new ArrayList<>();
                for(Documento d:documentos){
                        if(ruc==d.getRuc()){
                                listabuscadaruc.add(d);
                        }
                }
                return listabuscadaruc;
        }

        //busquedas mixtas

        public ArrayList<Documento> Busquedaxtorrextipo(String torre,String tipo) {
               ArrayList<Documento>  listadotorretipo=new ArrayList<>();
                for(Documento d:documentos){
                        if(d.getTipo().equals(tipo) && d.getTorre().equals(torre)){
                                listabuscadaruc.add(d);
                        }
                }
                return listabuscadaruc;
        }
        public ArrayList<Documento> Busquedaxtorrexcancelado(String torre,String cancelado) {
                ArrayList<Documento>  listadotorretipo=new ArrayList<>();
                for(Documento d:documentos){
                        if(d.getCancelado().equals(cancelado) && d.getTorre().equals(torre)){
                                listabuscadaruc.add(d);
                        }
                }
                return listabuscadaruc;
        }
        public ArrayList<Documento> Busquedaxperiodo(int mes,int anio) {
                String periodo= ""+ anio + String.format("%02d", mes);
                listabuscadaperiodo =new ArrayList<>();
                for(Documento d:documentos){
                         if(periodo.equals(d.getPeriodo())){
                                 listabuscadaperiodo.add(d);
                        }
                }
                return listabuscadaperiodo;
        }

        public ArrayList<Documento> Busquedaxperiodoxcancelado(int mes,int anio,String cancelado) {
                String periodo= ""+ anio + String.format("%02d", mes);
                listabuscadaperiodo=new ArrayList<>();
                for(Documento d:documentos){
                        if(periodo.equals(d.getPeriodo()) && cancelado.equals(d.getCancelado())){
                                listabuscadaperiodo.add(d);
                        }
                }
                return listabuscadaperiodo;
        }
        public ArrayList<Documento> Busquedaxperiodoxtorre(int mes,int anio,String torre) {
                String periodo= ""+ anio + String.format("%02d", mes);
                listabuscadaperiodo=new ArrayList<>();
                for(Documento d:documentos){
                        if(periodo.equals(d.getPeriodo()) && torre.equals(d.getTorre())){
                                listabuscadaperiodo.add(d);
                        }
                }
                return listabuscadaperiodo;
        }
        public ArrayList<Documento> Busquedaxperiodoxtorrexcancelado(int mes,int anio,String torre,String cancelado) {
                String periodo= ""+ anio + String.format("%02d", mes);
                listabuscadaperiodo=new ArrayList<>();
                for(Documento d:documentos){
                        if(periodo.equals(d.getPeriodo()) && torre.equals(d.getTorre())&&cancelado.equals(d.getCancelado())){
                                listabuscadaperiodo.add(d);
                        }
                }
                return listabuscadaperiodo;
        }
                        //====================fin busquedas mixtas======================

        // ******* CALCULO DE MONTO TOTAL DE GASTO EN UN PERIODO - MES PARA EL CONDOMINIO COMPLETO
        public double TotalxPeriodo(String periodo){
                //String periodo= ""+ anio + String.format("%02d", mes);;
                double total=0.0;

                for(Documento d:documentos) {
                        double monto=0.0;
                        if(periodo.equals(d.getPeriodo())) {
                                monto=d.getMontoTotal();
                        }
                        total=total+monto;
                }
                return total;
        }


        // ************ CALCULO PRINCIPAL PARA EMITIR LOS RECIBOS **********
        public static double TotalxPeriodoxtorre(String periodo, String torre){
                // int mes,int anio
                //String periodo= ""+ anio + String.format("%02d", mes);
                double total=0.0;
                for(Documento d: GastoComun.documentos) {
                        double monto=0.0;
                      if(periodo.equals(d.getPeriodo()) && torre.equals(d.getTorre())) {
                              monto=d.getMontoTotal();
                       }
                        total=total+monto;
                }
                return total;
        }

        public double TotalxPeriodoxtorrexCancelado(int mes,int anio,String torre,String cancelado){
                String periodo= ""+ anio + String.format("%02d", mes);
                double total=0.0;

                for(Documento d:documentos) {
                        double monto=0.0;
                        if(periodo.equals(d.getPeriodo()) && torre.equals(d.getTorre()) && cancelado.equals(d.getCancelado()) ) {
                                monto=d.getMontoTotal();
                        }
                        total=total+monto;
                }
                return total;
        }

}
