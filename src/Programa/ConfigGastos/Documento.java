package Programa.ConfigGastos;

public class Documento extends DetalleDocumento {

    private String tipo;
    private String cancelado;
    private String torre; // ingresar torre que corresponda o comun para gastos a prorratear en "el condominio"
    private String numeroDocumento;
    private String periodo;
    private int fechaFact;
    private int fechaVenc;
    private int ruc;
    private String nombre;
    private Double igv;
    private Double montoTotal;
    private String fechaCreacion;
    private String fechaEliminacion;


    public Documento( String tipo,String numeroDocumento,String periodo, int fechaFact,int fechaVenc,  int ruc, String nombre,String torre,
                     String descripcion, Double cantidad, Double precio,
                       String fechaCreacion) {
        super(descripcion, cantidad, precio);
        this.tipo=tipo;
        this.numeroDocumento = numeroDocumento;
        this.periodo=periodo;
        this.fechaFact = fechaFact;
        this.ruc = ruc;
        this.nombre = nombre;
        this.torre=torre;
        this.igv = Calcigv();
        this.montoTotal = Calcmontotal();
        this.fechaCreacion = fechaCreacion;
        this.fechaEliminacion = "no";
        this.fechaVenc=fechaVenc;
        this.cancelado="no";
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public int getFechaFact() {
        return fechaFact;
    }

    public void setFechaFact(int fechaFact) {
        this.fechaFact = fechaFact;
    }

    public int getRuc() {
        return ruc;
    }

    public void setRuc(int ruc) {
        this.ruc = ruc;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getIgv() {
        return igv;
    }


    public Double getMontoTotal() {
        return montoTotal;
    }


    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getFechaEliminacion() {
        return fechaEliminacion;
    }

    public void setFechaEliminacion(String fechaEliminacion) {
        this.fechaEliminacion = fechaEliminacion;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCancelado() {
        return cancelado;
    }

    public void setCancelado(String cancelado) {
        this.cancelado = cancelado;
    }


    public String getTorre() {
        return torre;
    }

    public void setTorre(String torre) {
        this.torre = torre;
    }

    public int getFechaVenc() {
        return fechaVenc;
    }

    public void setFechaVenc(int fechaVenc) {
        this.fechaVenc = fechaVenc;
    }

    @Override
    public String toString() {
        return "Documento{" +
                "tipo='" + tipo + '\'' +
                ", cancelado='" + cancelado + '\'' +
                ", torre='" + torre + '\'' +
                ", numeroDocumento='" + numeroDocumento + '\'' +
                ", periodo=" + periodo +
                ", fechaFact=" + fechaFact +
                ", fechaVenc=" + fechaVenc +
                ", ruc=" + ruc +
                ", nombre='" + nombre + '\'' +
                    super.toString()+
                ", igv=" + igv +
                ", montoTotal=" + montoTotal +
                ", fechaCreacion='" + fechaCreacion + '\'' +
                ", fechaEliminacion='" + fechaEliminacion + '\'' +
                '}'+'\n';
    }

    public Double Calcigv(){
        igv = super.CalcSubtotal()*0.18;

        return igv;
    }
    public Double Calcmontotal(){
        montoTotal = super.CalcSubtotal()+Calcigv();
        return montoTotal;
    }

}
