package Programa.ConfigPersona;
import Programa.ConfigCondominio.Cochera;
import Programa.ConfigCondominio.Departamento;
import Programa.Excepciones.ExcepcionNoEncuentraValorBuscado;

import java.util.Scanner;

public class Persona {
    String tipodocpersona;
    String numdocpersona;
    String nombrepersona;
    String apellpatpersona;
    String apellmatpersona;
    String nacionpersona;
    String sexopersona;
    String correoelectronico;
    String telefonomovil;
    String telefonofijo;
    //private static ArrayList<Persona> listapersonas = new ArrayList<>();

    public Persona(String tipodocpersona, String numdocpersona, String nombrepersona, String apellpatpersona, String apellmatpersona, String nacionpersona, String sexopersona, String correoelectronico, String telefonomovil, String telefonofijo) {
        this.tipodocpersona = tipodocpersona;
        this.numdocpersona = numdocpersona;
        this.nombrepersona = nombrepersona;
        this.apellpatpersona = apellpatpersona;
        this.apellmatpersona = apellmatpersona;
        this.nacionpersona = nacionpersona;
        this.sexopersona = sexopersona;
        this.correoelectronico = correoelectronico;
        this.telefonomovil = telefonomovil;
        this.telefonofijo = telefonofijo;
        //listapersonas.add(this);
    }

    public String getTipodocpersona() {
        return tipodocpersona;
    }

    public void setTipodocpersona(String tipodocpersona) {
        this.tipodocpersona = tipodocpersona;
    }

    public String getNumdocpersona() {
        return numdocpersona;
    }

    public void setNumdocpersona(String numdocpersona) {
        this.numdocpersona = numdocpersona;
    }

    public String getNombrepersona() {
        return nombrepersona;
    }

    public void setNombrepersona(String nombrepersona) {
        this.nombrepersona = nombrepersona;
    }

    public String getApellpatpersona() {
        return apellpatpersona;
    }

    public void setApellpatpersona(String apellpatpersona) {
        this.apellpatpersona = apellpatpersona;
    }

    public String getApellmatpersona() {
        return apellmatpersona;
    }

    public void setApellmatpersona(String apellmatpersona) {
        this.apellmatpersona = apellmatpersona;
    }

    public String getNacionpersona() {
        return nacionpersona;
    }

    public void setNacionpersona(String nacionpersona) {
        this.nacionpersona = nacionpersona;
    }

    public String getSexopersona() {
        return sexopersona;
    }

    public void setSexopersona(String sexopersona) {
        this.sexopersona = sexopersona;
    }

    public String getCorreoelectronico() {
        return correoelectronico;
    }

    public void setCorreoelectronico(String correoelectronico) {
        this.correoelectronico = correoelectronico;
    }

    public String getTelefonomovil() {
        return telefonomovil;
    }

    public void setTelefonomovil(String telefonomovil) {
        this.telefonomovil = telefonomovil;
    }

    public String getTelefonofijo() {
        return telefonofijo;
    }

    public void setTelefonofijo(String telefonofijo) {
        this.telefonofijo = telefonofijo;
    }

    @Override
    public String toString() {
        return "Persona{" +
                "tipodocpersona='" + tipodocpersona + '\'' +
                ", numdocpersona='" + numdocpersona + '\'' +
                ", nombrepersona='" + nombrepersona + '\'' +
                ", apellpatpersona='" + apellpatpersona + '\'' +
                ", apellmatpersona='" + apellmatpersona + '\'' +
                ", nacionpersona='" + nacionpersona + '\'' +
                ", sexopersona='" + sexopersona + '\'' +
                ", correoelectronico='" + correoelectronico + '\'' +
                ", telefonomovil='" + telefonomovil + '\'' +
                ", telefonofijo='" + telefonofijo + '\'' +
                '}';
    }

    public static void encontrarPropietario(String numdocpersona) throws ExcepcionNoEncuentraValorBuscado {
        boolean encontrado = false;
        for (Departamento d : Departamento.getDpto_sector()) {
            if (d.getPropietario_dpto().equals(numdocpersona)) {
                System.out.println("Se han encontrado los datos para la persona con número de documento " + numdocpersona + " y son los siguientes \n" + d.toString());
                encontrado = true;
                break;
            }
        }
        if (!encontrado) {
            throw new ExcepcionNoEncuentraValorBuscado("El número de documento " + numdocpersona + " no está registrado");
        }
    }







    }

