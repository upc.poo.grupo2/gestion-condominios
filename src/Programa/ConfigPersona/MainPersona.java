package Programa.ConfigPersona;

import Programa.ConfigCondominio.Cochera;
import Programa.Excepciones.ExcepcionNoEncuentraValorBuscado;
import Programa.Excepciones.ExcepcionNumdocRegistrado;

import java.util.Scanner;

public class MainPersona {
    public static void main(String[] args) {
        ArregloPersona arregloPersona = new ArregloPersona();



            Scanner sc = new Scanner(System.in);

        /* datos de prueba */
            System.out.println("======= INGRESAR TIPO DE DOCUMENTO============");
            String tipodocpersona = sc.nextLine();
            System.out.println("==== INGRESAR NUMERO DE DOCUMENTO");
            String numdocpersona = sc.nextLine();
            System.out.println("==== INGRESAR NOMBRES");
            String nombrepersona = sc.nextLine();
            System.out.println("==== INGRESAR APELLIDO PARTERNO");
            String apellpatpersona = sc.nextLine();
            System.out.println("==== INGRESAR APELLIDO MATERNO");
            String apellidomatpersona = sc.nextLine();
            System.out.println("==== INGRESAR NACIONALIDAD");
            String nacionpersona = sc.nextLine();
            System.out.println("==== INGRESAR SEXO F/M");
            String sexopersona = sc.nextLine();
            System.out.println("==== INGRESAR correoelectrónico");
            String correoelectronico = sc.nextLine();
            System.out.println("==== INGRESAR telefono movil");
            String telefonomovil = sc.nextLine();
            System.out.println("==== INGRESAR telefono fijo");
            String telefonofijo = sc.nextLine();

        try {  arregloPersona.registrarPersona(new Persona(tipodocpersona,numdocpersona,nombrepersona,apellpatpersona,apellidomatpersona,nacionpersona,sexopersona,correoelectronico,telefonomovil,telefonofijo));



     //       arregloPersona.registrarPersona(new Persona("A","1234","Juan","Perez","Ramirez","vemezolana","M","juanupc@gmail.com","999666999","3334444"));
     //       arregloPersona.registrarPersona(new Persona("DNI", "22221111", "Brillitt", "Ccasa", "Rodas", "peruana", "F", "brillitt123@outlook.com", "958554665", "48552525"));

 //     //PRUEBA DE REGISTRO DE UN DNI QUE YA SE ENCONTRABA REGISTRADO
     //       arregloPersona.registrarPersona(new Persona("DNI", "22221111", "Brillitt", "Ccasa", "Rodas", "peruana", "F", "brillitt123@outlook.com", "958554665", "48552525"));
        } catch (ExcepcionNumdocRegistrado ex) {
            System.out.println(ex.getMessage());
        }


//      //LISTA DATOS AGREGADOS
        arregloPersona.listarPersonas();

//      //PRUEBA CON DOCUMENTO NO REGISTRADO
        try {            arregloPersona.encontrarPersona("88884444");
        } catch (ExcepcionNoEncuentraValorBuscado ex) {System.out.println(ex.getMessage());        }

//      //PRUEBA CON DOCUMENTO SI REGISTRADO
        try {            arregloPersona.encontrarPersona("22221111");
        } catch (ExcepcionNoEncuentraValorBuscado ex) {            System.out.println(ex.getMessage());        }




    }
}

