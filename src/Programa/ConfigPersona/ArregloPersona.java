package Programa.ConfigPersona;

import Programa.Excepciones.ExcepcionNoEncuentraValorBuscado;
import Programa.Excepciones.ExcepcionNumdocRegistrado;

import java.util.ArrayList;

public class ArregloPersona {
    static ArrayList<Persona> personas;

    public ArregloPersona (){ this.personas = new ArrayList<>();}

    public void registrarPersona(Persona persona) throws ExcepcionNumdocRegistrado {
        String NumdocBuscado = persona.getNumdocpersona();
        for (Persona p : personas) {
            if (NumdocBuscado.equals(p.getNumdocpersona())) {
                throw new ExcepcionNumdocRegistrado(
                        "El documento " + NumdocBuscado + " de " +
                                persona.getNombrepersona() + " ya estaba registrado."
                );
            }
        }
        personas.add(persona);
    }

    public static void listarPersonas(){
        for (Persona i: personas){
            System.out.println(i);
        }
    }

    public static void encontrarPersona(String numdocpersona) throws ExcepcionNoEncuentraValorBuscado {
        boolean encontrado = false;
        for (Persona p: personas) {
            if (numdocpersona.equals(p.getNumdocpersona())){
                System.out.println("Se han encontrado los datos para la persona con número de documento "+numdocpersona+" y son los siguientes \n" + p.toString());
                encontrado = true;
                break;
            }
        }
        if (!encontrado){
            throw new ExcepcionNoEncuentraValorBuscado("El número de documento "+numdocpersona+" no está registrado");
        }


    }




}
