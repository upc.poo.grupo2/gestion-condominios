package Programa.Excepciones;

import java.util.Scanner;

public class ExcepcionEsperar {

    static public void ExcepcionEsperarTeclado() {
        String seguir;
        Scanner teclado = new Scanner(System.in);
        System.out.println("Presionar cualquier tecla para seguir...");
        try
        {
            seguir = teclado.nextLine();
        }
        catch(Exception e)
        {}
    }

}
