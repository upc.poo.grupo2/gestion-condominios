package Programa.Excepciones;

public class ExcepcionRecibosEmitidos extends Exception{
    public ExcepcionRecibosEmitidos(String mensaje) {
        super(mensaje);
    }
}
