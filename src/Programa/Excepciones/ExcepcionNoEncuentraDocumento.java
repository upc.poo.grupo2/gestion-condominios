package Programa.Excepciones;

public class ExcepcionNoEncuentraDocumento extends Exception {
    public ExcepcionNoEncuentraDocumento(String mensaje) {
        super(mensaje);
    }
}
