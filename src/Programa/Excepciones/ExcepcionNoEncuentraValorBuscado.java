package Programa.Excepciones;

public class ExcepcionNoEncuentraValorBuscado extends RuntimeException {
    public ExcepcionNoEncuentraValorBuscado(String mensaje) {
        super(mensaje);
    }
}

