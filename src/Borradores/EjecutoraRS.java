package Borradores;

import Programa.ConfigCondominio.*;
import Programa.Excepciones.ExcepcionNoEncuentraValorBuscado;
import Programa.ConfigPersona.Persona;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class EjecutoraRS {
    public static void main(String[] args) {

        Departamento a102 = new Departamento("C01", "a", "101", 71.5, "22221111");
        Departamento b101 = new Departamento("C01", "b", "101", 71.5, "PROPIETARIO2");
        Departamento c103 = new Departamento("C02", "c", "103", 71.5, "PROPIETARIO3");
        Departamento a104 = new Departamento("C02", "a", "104", 75.5, "PROPIETARIO4");

        Cochera est1001 = new Cochera("C02", "est", "1001", "PROPIETARIO3");
        Cochera est1002 = new Cochera("C02", "est", "1002", "PROPIETARIO4");

        // asignar usuarios depto y/o cochera & asignar dpto_ref a cada cochera
        est1001.setUsuario_cochera("Ricardo");
        est1001.setDpto_ref("a104");
        est1002.setDpto_ref("a104");

        // definir sectores
        Sector torreA = new Sector("c001", "A", "TORRE A", 20);
        Sector torreB = new Sector("c001", "B", "TORRE B", 15);

        // definir condominio
        Condominio Condom_c001 = new Condominio("c001", "alto colonial", "jr zorritos");


        AreaComun juegos1 = new AreaComun("C01", "salas", "sala juegos 1");


        // metodo para listar arreglos de dptos
        System.out.println(Departamento.getDpto_sector());
        // metodo para listar arreglos de cocheras
        System.out.println(Cochera.getCochera_sector());
        // metodo para listar zonas comunes
        System.out.println(AreaComun.getZonas_comunes());

        // juntar arreglos
        ArrayList<Condominio> lista = new ArrayList<>();

        lista.addAll(Departamento.getDpto_sector());
        lista.addAll(Cochera.getCochera_sector());
        lista.addAll(AreaComun.getZonas_comunes());

        //imprime condominio
        System.out.println(lista);

        System.out.println("-----------------");
        // revisar que codigo de condominio tienen¿ cada deptO
        for (Condominio x : lista) {
            if (x instanceof Departamento) {
                System.out.println(x.getCodigo_condominio());
            }
        }
        System.out.println("-----------------");
        // de sectores o torres / blocks del condominio
        System.out.println(Sector.getLista_sectores().size());

        //ArrayList<Sector> test = Sector.getLista_sectores();
        // cuanto depa tiene la torre a
        int num_dp = 0;

        System.out.println("-----------------");
        // metodo para num dptos por torre
        System.out.println(torreA.num_dptosSector("A"));
        // metodo para todos los dptos en el arreglo sectores
        System.out.println(Sector.num_dptos());

        System.out.println("-----------------");


        System.out.println(Departamento.getDpto_sector());

        System.out.println("-----------------");

        Condom_c001.informacionGeneral();
        System.out.println(lista);

        System.out.println("-----------------");

        ///a104.setFactor(0.111);
        System.out.println(Departamento.getDpto_sector());


        //ArrayList<Departamento> Departamento.getDpto_sector() --- test2;
        /*
        Arraylist<String> dni_validar = new Arralist<>;
        for (Departamento objDpto : test2) {
        dni_validar.add(objDto.getPropietario_dpto()
        dni_valiadr.add(objDto.getgetHabitante_dpto()
        }


        dpto = "" + objDpto.getSector() + objDpto.getNumero_dpto();
        cantcochera = 0;
        areacocheras = 0.0;
        cocheras = "|";
        //System.out.println(dpto);
        for (Cochera objCochera : test3) {
            if (objCochera.getDpto_ref().equals(dpto)) {
                cantcochera += 1;
                areacocheras += objCochera.getArea_cochera();
                cocheras += objCochera.getNumero_cochera() + "|";
         */

        try {
            Persona.encontrarPropietario("22221111");
        } catch (ExcepcionNoEncuentraValorBuscado ex) {
            System.out.println(ex.getMessage());
        }


        // metodo actualizar propietario ("a","104","41609203")

        try {
            for (Departamento objDpto : Departamento.getDpto_sector()) {
                if (objDpto.getSector().equals("a") && objDpto.getNumero_dpto().equals("104")) {
                    objDpto.setHabitante_dpto("41609203");
                }
            }
        } catch (ExcepcionNoEncuentraValorBuscado ex) {
            System.out.println(ex.getMessage());
        }

        // imprimir condominio

        System.out.println(lista);

        // ******** CARGA MASIVA DE dPTOS *********
        String ruta_archivo1 = "D:/DatosDpto.csv";
        String[][] tablaDpto = new String[0][];
        try {
            tablaDpto = CargarDpto.cargar_array_csv(ruta_archivo1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(Arrays.deepToString(tablaDpto));
    }


}
