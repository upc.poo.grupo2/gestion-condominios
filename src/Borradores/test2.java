package Borradores;

import Programa.ConfigCondominio.*;
import Programa.ConfigGastos.Documento;
import Programa.Excepciones.ExcepcionDocumentoduplicado;
import Programa.Excepciones.ExcepcionNoEncuentraDocumento;
import Programa.Excepciones.ExcepcionRecibosEmitidos;
import Programa.ConfigGastos.GastoComun;

public class test2 {
    public static void main(String[] args) {
        Sector torreA = new Sector("C01", "a", "TORRE A", 10);
        Sector torreB = new Sector("C01", "b", "TORRE B", 8);

        Departamento a101 = new Departamento("C01", "a", "101", 71.5, "22221111");
        Departamento b101 = new Departamento("C01", "b", "101", 71.5, "PROPIETARIO2");
        Departamento b103 = new Departamento("C01", "b", "103", 71.5, "PROPIETARIO3");
        Departamento a104 = new Departamento("C01", "a", "104", 75.5, "PROPIETARIO4");

        Cochera est1001 = new Cochera("C01", "est", "1001", "PROPIETARIO3");
        Cochera est1002 = new Cochera("C01", "est", "1002", "PROPIETARIO4");

        // asignar usuarios depto y/o cochera & asignar dpto_ref a cada cochera
        est1001.setUsuario_cochera("Ricardo");
        est1001.setDpto_ref("a104");
        est1002.setDpto_ref("a104");

        // definir sectores
        //Sector torreA = new Sector("c001", "A", "TORRE A", 20);
        //Sector torreB = new Sector("c001", "B", "TORRE B", 15);

        // definir condominio
        Condominio Condom_c001 = new Condominio("C01", "alto colonial", "jr zorritos");


        AreaComun juegos1 = new AreaComun("C01", "salas", "sala juegos 1");

        System.out.println(Sector.num_dptos());
        GastoComun gastoComun=new GastoComun();

        try {
            gastoComun.RegistrarDocumento(new Documento("mantto", "001", "2109", 22092011, 30092011, 2010900770, "CCU sac", "a", "gasto1", 1.0, 50.0, "22/09/2021"));
            gastoComun.RegistrarDocumento(new Documento("luz", "002", "2109", 30082011, 30092011, 2010900770, "CCU sac", "comun", "gasto2", 1.0, 75.0, "22/09/2021"));
            gastoComun.RegistrarDocumento(new Documento("luz", "003", "2109", 15082011, 30092011, 2035221443, "CCU sac", "a", "gasto3", 1.0, 100.0, "22/09/2021"));
            gastoComun.RegistrarDocumento(new Documento("servicio", "142", "2109", 15082011, 30092011, 2035221443, "CCU sac", "comun", "Jardineria", 4.0, 130.0, "23/09/2021"));
            gastoComun.RegistrarDocumento(new Documento("luz", "004", "2109", 15082011, 30092011, 2035221410, "CCU sac", "b", "gasto4", 1.0, 130.0, "23/09/2021"));
            gastoComun.RegistrarDocumento(new Documento("agua", "005", "2109", 15082011, 30092011, 2035221410, "CCU sac", "b", "gasto5", 1.0, 45.0, "23/09/2021"));
            gastoComun.RegistrarDocumento(new Documento("agua", "006", "2109", 15082011, 30092011, 2035221411, "CCU sac", "a", "gasto6", 1.0, 90.0, "23/09/2021"));
            gastoComun.RegistrarDocumento(new Documento("servicio", "007", "2109", 15082011, 30092011, 2035221411, "CCU sac", "a", "gasto7", 1.0, 23.0, "23/09/2021"));
            gastoComun.RegistrarDocumento(new Documento("servicio", "008", "2109", 15082011, 30092011, 2035221412, "CCU sac", "b", "gasto8", 1.0, 32.0, "23/09/2021"));

        } catch (ExcepcionDocumentoduplicado e)

    {
        System.out.println(e.getMessage());
    }
        // **************************************************************************************

        // *********  EMITIR RECIBOS PARA UN PERIODO --  NO PERMITE EMITIR PERIODOS YA GENERADOS
        try {
            GastoDepartamento.emitirRecibos("2109");
            GastoDepartamento.emitirRecibos("2108");
            GastoDepartamento.emitirRecibos("2108");
        }
        catch (ExcepcionRecibosEmitidos e) {
                System.out.println(e.getMessage());
            }

        // ********* BUSCAR RECIBOS DE UN PERIODO  *********************
        try {
            GastoDepartamento.buscarRecibo("2109","a","104");
        }
        catch (ExcepcionNoEncuentraDocumento e) {
            System.out.println(e.getMessage());
        }

        // ********* CAMBIAR ESTADO DE RECIBOS DE UN PERIODO  *********************
        try {
            GastoDepartamento.cambiarEstadoRecibo("2109","a","104","cancelado");
        }
        catch (ExcepcionNoEncuentraDocumento e) {
            System.out.println(e.getMessage());
        }

    }

}
